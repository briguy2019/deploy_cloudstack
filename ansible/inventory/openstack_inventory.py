#!/usr/bin/env python
import os_client_config
from collections import defaultdict


nova = os_client_config.make_client('compute')
def get_flavors():
  results = {}
  for flavor in nova.flavors.list():
    results.update({flavor.id:{'name':flavor.name}})
  return results 

def get_servers():
  results = {}
  for server in nova.servers.list():
    results.update({server.id:{'name':server.name,'status':server.status,'networks':server.networks, 'group': server.metadata['group']}})
  return results

#generate inventory
import yaml
import json

d = {}
ansiblegroups = []
#ansiblehosts = []

#create list of ansible groups from all instances' metadata
for server in nova.servers.list():
  ansiblegroups.append(server.metadata['group'])

for group in ansiblegroups:
  ansiblehosts = []
  for server in  get_servers():
    serveritem = get_servers()[server]
    ansiblehosts.append(serveritem['networks']['control-plane'][0])
  
  d.setdefault(group, {}).setdefault('hosts', ansiblehosts)

print json.dumps(d,sort_keys=False, indent=4)
